#include "Arduino.h"
#include "PB_dice.h"

#define ECHO_PIN 4//Ultrasound interface
#define TRIG_PIN 5

double distance_value = 0;


/* Realization of Ultrasound Ranging*/
int getDistance()
{
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG_PIN, LOW);
    return (int)pulseIn(ECHO_PIN, HIGH) / 58;
}

void setup()
{ 
    Serial.begin(9600);
    pinMode(ECHO_PIN, INPUT);
    pinMode(TRIG_PIN, OUTPUT);
}

void loop(){
  PBDice my_dice; 

  distance_value = getDistance();
  Serial.println(distance_value);

  if (distance_value >= 1 && distance_value <= 5) {
      my_dice.throw_dice();
      delay(100);
  }
}
