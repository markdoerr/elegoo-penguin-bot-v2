/*
A dice application for the Penguin Bot

 \authors mark doerr
 \date  191229

*/

#ifndef PB_dice_h
#define PB_dice_h

#include "Arduino.h"
#include "MY1690.h"

class PBDice
{
  public:
    PBDice(void);
    void throw_dice(void);

  private:
    MY1690_16S mp3;
    int _pin;
};

#endif
