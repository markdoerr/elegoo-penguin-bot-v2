/*
    MP3:Implementation of MP3 Driver
*/

#ifndef MY1690_h
#define MY1690_h

#include "Arduino.h"
#include "NeoSWSerial.h"



class MY1690_16S
{
public:
    int volume;
    String playStatus[5] = {"0", "1", "2", "3", "4"}; // STOP PLAYING PAUSE FF FR
    MY1690_16S();
    void playSong(unsigned char num, unsigned char vol);
    
    String getPlayStatus();
    String getStatus();
    void stopPlay();
    void setVolume(unsigned char vol);
    void volumePlus();
    void volumeDown();
    void setPlayMode(unsigned char mode);
    void checkCode(unsigned char *vs);
    void ampMode(int p, bool m);
    void init();
private:
    byte CMD_MusicPlay[5] = {0x7E, 0x03, 0x11, 0x12, 0xEF};
    byte CMD_MusicStop[5] = {0x7E, 0x03, 0x1E, 0x1D, 0xEF};
    byte CMD_MusicNext[5] = {0x7E, 0x03, 0x13, 0x10, 0xEF};
    byte CMD_MusicPrev[5] = {0x7E, 0x03, 0x14, 0x17, 0xEF};
    byte CMD_VolumePlus[5] = {0x7E, 0x03, 0x15, 0x16, 0xEF};
    byte CMD_VolumeDown[5] = {0x7E, 0x03, 0x16, 0x15, 0xEF};
    byte CMD_VolumeSet[6] = {0x7E, 0x04, 0x31, 0x00, 0x00, 0xEF};
    byte CMD_PlayMode[6] = {0x7E, 0x04, 0x33, 0x00, 0x00, 0xEF};
    byte CMD_SongSelet[7] = {0x7E, 0x05, 0x41, 0x00, 0x00, 0x00, 0xEF};
    byte CMD_getPlayStatus[5] = {0x7E, 0x03, 0x20, 0x23, 0xEF};
    //NeoSWSerial mp3Serial(SOFTWARE_RXD, SOFTWARE_TXD);
};

#endif
