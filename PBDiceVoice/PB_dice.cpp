/*
A dice application for the Penguin Bot

 \authors mark doerr
 \date  191229

*/

#include "Arduino.h"
#include "PB_dice.h"

PBDice::PBDice()
{
  MY1690_16S mp3;
  mp3.init();
   
  randomSeed(analogRead(0));
     
  delay(10);

  // say ready message
}

void PBDice::throw_dice()
{
  int randNumber = random(1,7);

  //Serial.print("Dice number is: ");

  switch (randNumber) {
  case 1:
    //Serial.println("1");
    mp3.playSong(31, mp3.volume);
    delay(2000);
    
    break;
  case 2:
    mp3.playSong(32, mp3.volume);
    delay(2000);
    
    break;
  case 3:
    mp3.playSong(33, mp3.volume);
    delay(2000);
    
    break;
  case 4:
    mp3.playSong(34, mp3.volume);
    delay(2000);
    
    break;

  case 5:
    mp3.playSong(35, mp3.volume);
    delay(2000);
    
    break;

   case 6:
    mp3.playSong(36, mp3.volume);
    delay(2000);
    
    break;
      
  default:
    // statements
    break;
  }
}
